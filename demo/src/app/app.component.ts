import {Component} from '@angular/core';
import {User} from "./User";
import {TodoItem} from "./TodoItem";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Todo List App';
  user: User = new User();
  items: TodoItem[] = [
    {job: "Water the flowers", isDone: false},
    {job: "Study Angular", isDone: false},
    {job: "Prepare API backend", isDone: false},
    {job: "Cook", isDone: false}
  ];

  constructor() {
    this.user.name = "John";
  }

  checkBoxChange(item: TodoItem) {
    console.log(item);
    console.log("value changed ! ...");
    this.items = this.items.filter(value => value.isDone === false);
  }

  addTodoJob(val: string): void {
    if (val.length === 0) {
      alert("job can not be empty");
      return;
    }
    let item: TodoItem = new TodoItem();
    item.isDone = false;
    item.job = val;
    this.items.push(item);
  }
}
