import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {Movie} from "../Movie";

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private http: HttpClient) {
  }

  public getMovies(): Observable<Movie[]> {
    return this.http.get<Movie[]>("http://localhost:8080/movies");
  }

  public postMovie(param:Movie) : Observable<Movie> {
    return this.http.post<Movie>("http://localhost:8080/movies",param);
  }
}
