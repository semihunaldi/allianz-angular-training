import {Component, OnInit} from '@angular/core';
import {Movie} from "./Movie";
import {MoviesService} from "./service/movies.service";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-movie',
  templateUrl: './movie.component.html',
  styleUrls: ['./movie.component.css']
})
export class MovieComponent implements OnInit {

  constructor(private moviesService: MoviesService, private route: ActivatedRoute) {
  }

  movies: Movie[] = [];
  selectedCategory: string;

  ngOnInit() {
    this.moviesService.getMovies().subscribe(data => this.movies = data);
    this.route.params.subscribe(x => this.selectedCategory = x.name);
  }

  getMovies() {
    return this.movies ?
      this.movies.filter(x => this.selectedCategory ? x.genre === this.selectedCategory : true)
      : null;
  }
}
