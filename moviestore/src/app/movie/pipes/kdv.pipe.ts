import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'kdv'
})
export class KdvPipe implements PipeTransform {
  transform(value: number, args?: number): any {
    let defaultKDV = args ? args : 0.18;
    return value * (1 + defaultKDV);
  }
}
