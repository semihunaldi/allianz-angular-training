import {Cast} from "./Cast";

export class Movie {
  id: number;
  name: string;
  director: string;
  casts?: Cast[];
  publishDate: Date;
  genre: string;
  price: number;
  cover: string = "https://via.placeholder.com/150";
}
