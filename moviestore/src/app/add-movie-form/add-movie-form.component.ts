import { Component, OnInit } from '@angular/core';
import {FormGroup, FormsModule} from "@angular/forms";
import {Movie} from "../movie/Movie";
import {Genre} from "../genre/Genre";
import {GenreService} from "../genre/service/genre.service";
import {MoviesService} from "../movie/service/movies.service";

@Component({
  selector: 'app-add-movie-form',
  templateUrl: './add-movie-form.component.html',
  styleUrls: ['./add-movie-form.component.css']
})
export class AddMovieFormComponent implements OnInit {

  constructor(private genreService : GenreService, private movieService : MoviesService) { }

  movie : Movie = new Movie();
  genres : Genre[] = [];

  addMovie(myForm: FormGroup) {
    console.log(myForm);
    console.log(this.movie);
    this.movieService.postMovie(this.movie).subscribe(x => console.log(x));
  }

  ngOnInit() {
    this.genreService.getGenres().subscribe(o => this.genres = o);
  }
}
