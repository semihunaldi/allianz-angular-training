import {Component, Input, OnInit} from '@angular/core';
import {Genre} from "./Genre";
import {GenreService} from "./service/genre.service";

@Component({
  selector: 'app-genre',
  templateUrl: './genre.component.html',
  styleUrls: ['./genre.component.css']
})
export class GenreComponent implements OnInit {

  @Input() testField: string = "";
  @Input() testFieldBoolean: boolean = false;

  constructor(private genreService: GenreService) {
  }

  genres: Genre[] = [];

  ngOnInit() {
    console.log( "testField : " + this.testField);
    console.log( "testFieldBoolean : " + this.testFieldBoolean);
    this.genreService.getGenres().subscribe(value => this.genres = value);
  }

}
