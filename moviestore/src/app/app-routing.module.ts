import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MovieComponent} from "./movie/movie.component";
import {AddMovieFormComponent} from "./add-movie-form/add-movie-form.component";

const routes: Routes = [
  {path: "movies", component: MovieComponent},
  {path: "", redirectTo: "movies", pathMatch: "full"},
  {path: "addMovie", component: AddMovieFormComponent},
  {path: "movie/category/:name", component:MovieComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
